from flask import jsonify
from app.services.database_service import Database

class Series:
    
    def __init__(self, serie, seasons, released_date, genre, imdb_rating):
        self.serie = serie.title()
        self.seasons = seasons
        self.released_date = released_date
        self.genre = genre.title()
        self.imdb_rating = imdb_rating
        self.cur = None
        self.conn = None


    @staticmethod
    def create_database():
        db = Database()
        db.connect()        

        db.execute("""
        CREATE TABLE IF NOT EXISTS ka_series (
            id BIGSERIAL PRIMARY KEY,
            serie VARCHAR(100) NOT NULL UNIQUE,
            seasons INTEGER NOT NULL,
            released_date DATE NOT NULL,
            genre VARCHAR(50) NOT NULL,
            imdb_rating FLOAT NOT NULL
        );
        """)

        db.close()

    def insert(self):        

        db = Database()
        db.connect()

        insert_query = """
            INSERT INTO ka_series (serie, seasons, released_date, genre, imdb_rating) 
            VALUES (%s,%s,%s,%s,%s) RETURNING id
        """
        data_to_insert = (self.serie, self.seasons, self.released_date, self.genre, self.imdb_rating)
        id = db.execute(insert_query, data_to_insert)  

        db.close()      

        return {
            "id": id,
            "serie": self.serie,
            "released_date": self.released_date,
            "genre": self.genre,
            "imdb_rating": self.imdb_rating
        }

    @staticmethod
    def get_series():
        db = Database()
        db.connect()

        db.execute("""
            SELECT * FROM ka_series
        """)

        output = db.get_data()

        db.close()

        return {"data": output}

    @staticmethod
    def get_single_serie(serie_id):
        db = Database()
        db.connect()

        output = db.get_data_by_id(serie_id)

        db.close()

        return {"data": output}
