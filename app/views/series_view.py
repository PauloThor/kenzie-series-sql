from app.services.errors_service import NotFoundError
from flask import Flask, request, jsonify
from app.services.series_service import Series

def series_view(app: Flask):

    @app.post('/series')
    def create():
        data = request.get_json()

        Series.create_database()

        new_series = Series(
            serie = data['serie'],
            seasons = data['seasons'],
            released_date = data['released_date'],
            genre = data['genre'],
            imdb_rating = data['imdb_rating']
        )
        
        output = new_series.insert()

        return output, 201

    
    @app.get('/series')
    def series():
        Series.create_database()

        output = Series.get_series()

        return output, 200

    
    @app.get('/series/<int:serie_id>')
    def select_by_id(serie_id):
        Series.create_database()

        try:
            output = Series.get_single_serie(serie_id)

            return output, 200
        except NotFoundError as err:
            return err.message, err.status

    return app