from flask import Flask

def init_app(app: Flask):
    from app.views.series_view import series_view
    series_view(app)

    return app